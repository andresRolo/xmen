# XMEN MELI
This maven application uses:
1. Spring Framework
2. H2 Database
3. Junit Test (94% coverage)
4. Swagger

There are two options to run it:

# 1. AWS

Access the documentation in http://18.217.145.205:9001/swagger-ui.html URL

Access the H2 database in http://18.217.145.205:9001/h2-console/login.jsp URL

# 2. CLONE AND DEPLOY

- execute: git clone https://gitlab.com/andresRolo/xmen.git in <folder>

- move to <folder> and execute: mvn clean install

- execute: docker build -t xmen:latest .

- docker run -p 9001:9001 -t xmen

Access the documentation in http://localhost:9001/swagger-ui.html URL

Access the H2 database in http://localhost:9001/h2-console/login.jsp URL

# H2 Database

Use the H2 database credentials:

url: jdbc:h2:mem:mutant

user: mutant

password: mutant

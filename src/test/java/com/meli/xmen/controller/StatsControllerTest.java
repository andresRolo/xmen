package com.meli.xmen.controller;

import com.meli.xmen.common.StatDTO;
import com.meli.xmen.dao.MutantDao;
import com.meli.xmen.dao.StatsDao;
import com.meli.xmen.service.StatsService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.BDDMockito.given;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(StatsController.class)
public class StatsControllerTest {
    @Autowired
    private MockMvc mvc;
    @MockBean
    private StatsService statsService;
    @MockBean
    private StatsDao statsDao;
    @MockBean
    private MutantDao mutantDao;

    @Test
    public void stats() throws Exception {
        StatDTO statDTO = new StatDTO();
        statDTO.setCountMutantDna(1);
        statDTO.setCountHumanDta(1);
        statDTO.setRatio(1);
        given(this.statsService.stats()).willReturn(statDTO);

        this.mvc.perform(post("/stats/stats")
                .contentType(APPLICATION_JSON))
                .andExpect(status().isOk());
    }
}

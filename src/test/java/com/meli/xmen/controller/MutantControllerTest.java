package com.meli.xmen.controller;

import com.meli.xmen.dao.MutantDao;
import com.meli.xmen.dao.StatsDao;
import com.meli.xmen.service.MutantService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import static org.mockito.BDDMockito.given;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@WebMvcTest(MutantController.class)
public class MutantControllerTest {

    @Autowired
	private MockMvc mvc;
    @MockBean
    private MutantService mutantService;
    @MockBean
    private StatsDao statsDao;
    @MockBean
    private MutantDao mutantDao;

	@Test
	public void isMutant() throws Exception {
		String dna = "ATGCGA,CAGTGC,TTATGT,AGAAGG,CCCCTA,TCACTG";
		given(this.mutantService.isMutant(dna)).willReturn(Boolean.TRUE);

		this.mvc.perform(post("/mutant/isMutant")
				.contentType(APPLICATION_JSON)
                .param("dna", dna))
				.andExpect(status().isOk());
	}
}

package com.meli.xmen.service;

import com.meli.xmen.common.StatDTO;
import com.meli.xmen.dao.MutantDao;
import com.meli.xmen.dao.StatsDao;
import com.meli.xmen.entity.Stats;
import com.meli.xmen.service.impl.MutantServiceImpl;
import com.meli.xmen.service.impl.StatsServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class MutantServiceTest {

	@InjectMocks
	private MutantServiceImpl mutantService;

	@Mock
	private MutantDao mutantDao;

	@Mock
	private StatsDao statsDao;


	@Test
	public void stats() throws Exception {
		String dna = "ATGCGA,CAGTGC,TTATGT,AGAAGG,CCCCTA,TCACTG";
		Stats stats = new Stats();
		stats.setDnaCount(1);
		stats.setDnaSpecie("MUTANT");
		stats.setId(2);
		when(statsDao.findById(2)).thenReturn(stats);

		Boolean isMutant = mutantService.isMutant(dna);

		assertEquals(Boolean.TRUE, isMutant);
	}
}

package com.meli.xmen.service;

import com.meli.xmen.common.StatDTO;
import com.meli.xmen.controller.StatsController;
import com.meli.xmen.dao.StatsDao;
import com.meli.xmen.entity.Stats;
import com.meli.xmen.service.impl.StatsServiceImpl;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class StatsServiceTest {

	@InjectMocks
	private StatsServiceImpl statsService;

	@Mock
	private StatsDao statsDao;


	@Test
	public void stats() throws Exception {
		List<Stats> statsList = new ArrayList<>();
		Stats stats = new Stats();
		stats.setDnaCount(1);
		stats.setDnaSpecie("MUTANT");
		stats.setId(2);
		statsList.add(stats);

		when(statsDao.findAll()).thenReturn(statsList);

		StatDTO statsDTO = statsService.stats();

		assertEquals(stats.getDnaCount(), statsDTO.getCountMutantDna());
	}
}

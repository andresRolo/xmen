package com.meli.xmen.repository;

import com.meli.xmen.dao.MutantDao;
import com.meli.xmen.dao.StatsDao;
import com.meli.xmen.entity.Stats;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = NONE)
public class StatsRepositoryTest {

	@Autowired
	private TestEntityManager entityManager;

	@Autowired
	private StatsDao statsRepository;

	@Test
	public void whenFindByDna() {
		Stats stats = new Stats();
		stats.setId(3);
		stats.setDnaCount(1);
		stats.setDnaSpecie("ALIEN");
		entityManager.persist(stats);
		entityManager.flush();

		Stats statsTest = statsRepository.findById(stats.getId());

		assertThat(statsTest.getDnaCount()).isEqualTo(stats.getDnaCount());
		assertThat(statsTest.getDnaSpecie()).isEqualTo(stats.getDnaSpecie());
		assertThat(statsTest.getId()).isEqualTo(stats.getId());
	}
}

package com.meli.xmen.repository;

import com.meli.xmen.dao.MutantDao;
import com.meli.xmen.entity.Mutant;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;

@ActiveProfiles("test")
@RunWith(SpringRunner.class)
@DataJpaTest
@AutoConfigureTestDatabase(replace = NONE)
public class MutantRepositoryTest {

	@Autowired
	private TestEntityManager entityManager;

	@Autowired
	private MutantDao mutantRepository;

	@Test
	public void whenFindByDna() {
		Mutant mutant = new Mutant();
		mutant.setDna("ATGCGA,CAGTGC,TTATGT,AGAAGG,CCCCTA,TCACTG");
		mutant.setMutant(Boolean.TRUE);
		entityManager.persist(mutant);
		entityManager.flush();

		Mutant mutantTest = mutantRepository.findByDna(mutant.getDna());

		assertThat(mutantTest.getDna()).isEqualTo(mutant.getDna());
		assertThat(mutantTest.isMutant()).isEqualTo(mutant.isMutant());
	}
}

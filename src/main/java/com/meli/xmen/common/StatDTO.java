package com.meli.xmen.common;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author arueda
 *
 */
public class StatDTO implements Serializable{

	private static final long serialVersionUID = 902398575731657623L;

	private int countMutantDna;
	private int countHumanDta;
	private double ratio;

	public StatDTO() {
	}

	public int getCountMutantDna() {
		return countMutantDna;
	}

	public void setCountMutantDna(int countMutantDna) {
		this.countMutantDna = countMutantDna;
	}

	public int getCountHumanDta() {
		return countHumanDta;
	}

	public void setCountHumanDta(int countHumanDta) {
		this.countHumanDta = countHumanDta;
	}

	public double getRatio() {
		return ratio;
	}

	public void setRatio(double ratio) {
		this.ratio = ratio;
	}
}

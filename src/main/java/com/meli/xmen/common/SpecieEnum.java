package com.meli.xmen.common;

import java.util.EnumSet;

/**
 *
 */
public enum SpecieEnum {

    HUMAN(1, "HUMAN"),
    MUTANT(2, "MUTANT");

    private final int value;
    private final String label;

    public int getValue() {
        return value;
    }

    SpecieEnum(int value, String label) {
        this.value = value;
        this.label = label;
    }
}

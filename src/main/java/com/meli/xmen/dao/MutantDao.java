package com.meli.xmen.dao;

import com.meli.xmen.entity.Mutant;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author arueda
 *
 */
public interface MutantDao extends CrudRepository<Mutant, String> {

	Mutant findByDna(String dna);

}

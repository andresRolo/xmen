package com.meli.xmen.dao;

import com.meli.xmen.entity.Mutant;
import com.meli.xmen.entity.Stats;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * @author arueda
 *
 */
public interface StatsDao extends CrudRepository<Stats, Integer> {

	Stats findById(int id);
}

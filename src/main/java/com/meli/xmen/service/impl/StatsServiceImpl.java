
package com.meli.xmen.service.impl;

import com.meli.xmen.common.SpecieEnum;
import com.meli.xmen.common.StatDTO;
import com.meli.xmen.dao.StatsDao;
import com.meli.xmen.entity.Stats;
import com.meli.xmen.service.StatsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Locale;

/**
 * @author arueda
 *
 */
@Service
public class StatsServiceImpl implements StatsService {

	@Autowired
	private StatsDao statsDao;

    DecimalFormat formatter = new DecimalFormat("#.##", new DecimalFormatSymbols(Locale.US));

	@Override
	public StatDTO stats() {
		StatDTO statsDTO = new StatDTO();
		Iterable<Stats> stats = this.statsDao.findAll();
		stats.forEach(stat -> {
			if(stat.getId() == SpecieEnum.HUMAN.getValue()){
				statsDTO.setCountHumanDta(stat.getDnaCount());
			} else if(stat.getId() == SpecieEnum.MUTANT.getValue()){
				statsDTO.setCountMutantDna(stat.getDnaCount());
			}
		});
        if(statsDTO.getCountHumanDta() == 0){
            statsDTO.setRatio(0); //Or return an exception
        } else {
            double ratio = (double)statsDTO.getCountMutantDna() / (double)statsDTO.getCountHumanDta();
            statsDTO.setRatio(Double.parseDouble(formatter.format(ratio)));
        }
		return statsDTO;
	}
}

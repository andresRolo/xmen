/**
 * 
 */
package com.meli.xmen.service.impl;

import com.meli.xmen.common.SpecieEnum;
import com.meli.xmen.dao.MutantDao;
import com.meli.xmen.dao.StatsDao;
import com.meli.xmen.entity.Mutant;
import com.meli.xmen.entity.Stats;
import com.meli.xmen.service.MutantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * @author arueda
 *
 */
@Service
public class MutantServiceImpl implements MutantService {

    @Autowired
    private MutantDao mutantDao;
    @Autowired
    private StatsDao statsDao;

	static char[][] matrix;
	static List<String> mutantDNAs;
	static int sizeMatrix;
	static int totalMatch;
	static final int[] x = {-1, -1, -1, 0, 0, 1, 1, 1};
	static final int[] y = {-1, 0, 1, -1, 1, -1, 0, 1};


	@Override
	public boolean isMutant(String dna) {

        boolean isMutant;
        populateMutantDNA();
		matrix = arrayToMatrix(dna);
		sizeMatrix = dna.split(",").length;
        patternSearch(matrix);
		if(totalMatch / 2 > 1){
            isMutant = Boolean.TRUE;
		} else {
            isMutant = Boolean.FALSE;
		}
        checkIfDnaExists(dna.toString(), isMutant);
        return isMutant;

	}

    private void checkIfDnaExists(String dna, boolean isMutant) {
        Mutant mutant = this.mutantDao.findByDna(dna);
        if(mutant == null){
            Mutant newMutant = new Mutant();
            newMutant.setDna(dna);
            newMutant.setMutant(isMutant);
            this.mutantDao.save(newMutant);
            Stats stats = null;
            if(isMutant){
                stats = this.statsDao.findById(SpecieEnum.MUTANT.getValue());
            } else {
                stats = this.statsDao.findById(SpecieEnum.HUMAN.getValue());
            }
            stats.setDnaCount(stats.getDnaCount() + 1);
            this.statsDao.save(stats);
        }
    }

    private void populateMutantDNA() {
		mutantDNAs = new ArrayList<>( Arrays.asList("AAAA", "TTTT", "CCCC", "GGGG"));
	}

	private int patternSearch(char[][] grid){
        totalMatch = 0;
        mutantDNAs.stream().forEach(currentDNA -> {
            for (int row = 0; row < sizeMatrix; row++) {
                for (int col = 0; col < sizeMatrix; col++) {
                    if (searchDNA(grid, row, col, currentDNA)) {
                        totalMatch++;
                        System.out.println("pattern found at " + row + ", " + col);
                    }
                }
            }
        });
        return totalMatch;
	}

	private boolean searchDNA(char[][] grid, int row, int col, String word) {
		if (grid[row][col] != word.charAt(0))
			return false;

		int len = word.length();
		for (int dir = 0; dir < 8; dir++) {
			int k, rd = row + x[dir], cd = col + y[dir];
			for (k = 1; k < len; k++) {
				if (rd >= sizeMatrix || rd < 0 || cd >= sizeMatrix || cd < 0)
					break;
                if (grid[rd][cd] != word.charAt(k))
					break;
                rd += x[dir];
				cd += y[dir];
			}
			if (k == len)
				return true;
		}
		return false;
	}

	private char[][] arrayToMatrix(String dna) {
        String[] dnaArray = dna.split(",");
		char matrix[][] = new char[dnaArray.length][dnaArray.length];
		for (int j = 0; j < dnaArray.length; j++) {
			String currentLine = dnaArray[j];
			for (int i = 0; i < currentLine.length(); i++) {
                if(i >= dnaArray.length){
                    break;
                }
				matrix[j][i] = currentLine.charAt(i);
			}
		}
		return matrix;
	}
}

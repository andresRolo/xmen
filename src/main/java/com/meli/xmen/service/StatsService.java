package com.meli.xmen.service;

import com.meli.xmen.common.StatDTO;

/**
 * @author arueda
 *
 */
public interface StatsService {

	StatDTO stats();

}

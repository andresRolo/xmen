package com.meli.xmen.service;

/**
 * @author arueda
 *
 */
public interface MutantService {

	boolean isMutant(String dna);

}

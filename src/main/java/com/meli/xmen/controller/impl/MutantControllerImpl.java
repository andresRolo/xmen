package com.meli.xmen.controller.impl;

import com.meli.xmen.controller.MutantController;
import com.meli.xmen.service.MutantService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author arueda
 *
 */
@RestController
public class MutantControllerImpl implements MutantController {

    @Autowired
	private MutantService mutantService;

	@Override
	public boolean isMutant(String dna) {
		return this.mutantService.isMutant(dna);
	}
}

/**
 * 
 */
package com.meli.xmen.controller.impl;

import com.meli.xmen.common.StatDTO;
import com.meli.xmen.controller.StatsController;
import com.meli.xmen.service.StatsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author arueda
 *
 */
@RestController
public class StatsControllerImpl implements StatsController {

	private StatsService statsService;

	@Autowired
	public StatsControllerImpl(StatsService statsService) {
		this.statsService = statsService;
	}

	@Override
	public StatDTO stats() {
		return this.statsService.stats();
	}
}

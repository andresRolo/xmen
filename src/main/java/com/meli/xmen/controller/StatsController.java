package com.meli.xmen.controller;

import com.meli.xmen.common.StatDTO;
import io.swagger.annotations.*;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @author arueda
 *
 */
@ResponseBody
@RequestMapping(StatsController.STATS)
@Api(value = StatsController.VALUE,
		tags = {StatsController.VERSION})
public interface StatsController {

	String VERSION = "/1.0";
	String STATS = "/stats";
	String VALUE = "Stats Service";

	@PostMapping("/stats")
	@ApiOperation(
			value = "get stats of mutants and no mutants"
    )
	@ApiResponses(
			value = {
					@ApiResponse(code = 200, message = "OK", response = StatDTO.class)
			}
	)
	StatDTO stats();

}

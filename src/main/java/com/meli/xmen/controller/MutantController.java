package com.meli.xmen.controller;

import io.swagger.annotations.*;
import org.springframework.web.bind.annotation.*;

/**
 * @author arueda
 *
 */
@ResponseBody
@RequestMapping(MutantController.MUTANT)
@Api(value = MutantController.VALUE,
		tags = {MutantController.VERSION})
public interface MutantController {

    String VERSION = "/1.0";
    String MUTANT = "/mutant";
    String VALUE = "Mutant Service";

	@PostMapping("/isMutant")
	@ApiOperation(
			value = "check if there's a mutant"
    )
	@ApiResponses(
			value = {
					@ApiResponse(code = 200, message = "OK", response = Boolean.class)
			}
	)
	boolean isMutant(@ApiParam(value = "dna", required = true) @RequestParam String dna);

}

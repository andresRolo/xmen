package com.meli.xmen.config;

import static springfox.documentation.builders.PathSelectors.regex;

import com.meli.xmen.controller.MutantController;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * arueda
 */
@Configuration
@EnableSwagger2
public class Swagger2UiConfiguration
{
    private static final String ALL_CHILD = ".*";

    private ApiInfo metadata() {
        return new ApiInfoBuilder().title("Mutant MeLi").description("Mutant Service API")
                .termsOfServiceUrl("").contact(new Contact("", "", "andressdlm@gmail.com"))
                .version("1.0").build();
    }

    @Bean
    public Docket mutantApi() {
        return new Docket(DocumentationType.SWAGGER_2).groupName("Mutant API").apiInfo(metadata()).select()
                .paths(regex("/mutant" + ALL_CHILD)).build();

    }

    @Bean
    public Docket statsApi() {
        return new Docket(DocumentationType.SWAGGER_2).groupName("Stats API").apiInfo(metadata()).select()
                .paths(regex("/stats" + ALL_CHILD)).build();

    }
}
package com.meli.xmen.entity;

import com.meli.xmen.service.MutantService;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.RequestParam;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * @author arueda
 *
 */
@Entity
@Table(name="mutants")
public class Mutant implements Serializable{

	private static final long serialVersionUID = -5832898146577302701L;

	@Id
	private String dna;
	private boolean isMutant;

	public Mutant() {
	}

	public String getDna() {
		return dna;
	}

	public void setDna(String dna) {
		this.dna = dna;
	}

	public boolean isMutant() {
		return isMutant;
	}

	public void setMutant(boolean mutant) {
		isMutant = mutant;
	}
}

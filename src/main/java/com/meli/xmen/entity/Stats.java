package com.meli.xmen.entity;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.io.Serializable;

/**
 * @author arueda
 *
 */
@Entity
@Table(name="stats")
public class Stats implements Serializable{

	private static final long serialVersionUID = -6908256372600117589L;

	@Id
	private int id;
	private String dnaSpecie;
	private int dnaCount;

	public Stats() {
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getDnaSpecie() {
		return dnaSpecie;
	}

	public void setDnaSpecie(String dnaSpecie) {
		this.dnaSpecie = dnaSpecie;
	}

	public int getDnaCount() {
		return dnaCount;
	}

	public void setDnaCount(int dnaCount) {
		this.dnaCount = dnaCount;
	}

}
